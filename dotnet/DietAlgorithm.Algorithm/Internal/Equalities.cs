﻿using DietAlgorithm.Model;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex;
using System;

namespace DietAlgorithm
{
    public class Equalities
    {
        private ProjectedCgData data { get; set; }
        public Equalities(Matrix<double> G, Vector<double> c, Matrix<double> A, Vector<double> b, Vector<double> x0 = null)
        {
            var M = Matrix<double>.Build;

            if (x0 == null)
            {
                x0 = A.Inverse() * b;
            }

            var r = (G * x0) + c;
            var P = M.DenseIdentity(G.RowCount, G.ColumnCount);
            var g = P * r;
            var d = -g;

            data = new ProjectedCgData()
            {
                x = x0,
                r = r,
                g = g,
                d = d,
                P = P,
                G = G,
                A = A
            };
        }

        public Vector<double> Optimize(double threshold = 1e-4)
        {
            return Converge(data, threshold);
        }

        private Vector<double> Converge(ProjectedCgData data, double threshold)
        {
            var error = 1.0;

            while (true) {
                if (error < threshold)
                {
                    break;
                }

                Step(data, threshold);
                error = data.r * data.g;
            }

            return data.x;
        }

        private void Step(ProjectedCgData data, double threshold)
        {
            var alpha = CalculateAlpha(data, threshold);
            var x = data.x + alpha * data.d;
            var rprime = data.r + alpha * data.G * data.d;
            var gprime = data.P * rprime;
            var beta = rprime * gprime / (data.r * data.g);

            data.x = x;
            data.r = rprime;
            data.g = gprime;
            data.d = -gprime + beta * data.d;
        }

        private double CalculateAlpha(ProjectedCgData data, double threshold)
        {
            if (Math.Abs(data.d * data.G * data.d) < threshold)
            {
                return 0;
            }

            return data.r * data.g / (data.d * data.G * data.d);
        }
    }
}
