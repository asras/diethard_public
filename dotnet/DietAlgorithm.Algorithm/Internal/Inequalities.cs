﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;

namespace DietAlgorithm.Algorithm
{
    public class Inequalities
    {
        public Matrix<double> G;

        public Vector<double> c;

        public Inequalities(Matrix<double> G, Vector<double> c)
        {
            this.G = G;
            this.c = c;
        }
    }
}
