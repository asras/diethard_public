﻿using DietAlgorithm.Model;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;

namespace DietAlgorithm.Algorithm
{
    public static class RecipeOptimiser
    {
        public static List<Ingredient> Optimize(IList<Ingredient> ingredients, Target target)
        {
            var M = Matrix<double>.Build;
            var V = Vector<double>.Build;

            var A = M.Dense(ingredients.Count, ingredients.Count, 1.0);
            var b = V.Dense(ingredients.Count, 1.0);

            var inequalities = InitializeInequalities(ingredients.Count);

            var N = M.Dense(3, ingredients.Count, 0);
            for (var i = 0; i < ingredients.Count; i++)
            {
                N[1, i] = ingredients[i].Carbohydrates;
                N[2, i] = ingredients[i].Protein;
                N[3, i] = ingredients[i].Fat;
            }

            var y = V.Dense(3);
            y[1] = target.Carbohydrates;
            y[2] = target.Protein;
            y[3] = target.Fat;

            var G = 2.0 * N * N.Transpose();
            var c = -2.0 * y * N;

            var s = ActiveSet();

            var k = N * s
        }

        private static object InitializeInequalities(int count)
        {
            var M = Matrix<double>.Build;
            var V = Vector<double>.Build;

            var InequalitiesForA = M.DenseIdentity(count, count);
            var InequalitiesForB = V.Dense(count, 0);

            var inequalities = new List<Ineq>

            for (var i = 0; i < count; i++)
            {

            }
        }
    }
}
