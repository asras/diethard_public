﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DietAlgorithm.Model
{
    public class Target : Macronutrient
    {
        public Target(string name, double carbohydrates, double protein, double fat) : base(name, carbohydrates, protein, fat)
        {
        }
    }
}
