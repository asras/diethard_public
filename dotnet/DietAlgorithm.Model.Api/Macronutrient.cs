﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DietAlgorithm.Model
{
    public class Macronutrient
    {
        public double Carbohydrates { get; }
        public double Protein { get; }
        public double Fat { get; }
        public string Name { get; }

        public Macronutrient(double carbohydrates, double protein, double fat) : this("", carbohydrates, protein, fat)
        {
        }

        public Macronutrient(string name, double carbohydrates, double protein, double fat)
        {
            Name = name;
            ValidateInput(carbohydrates, protein, fat);
            Carbohydrates = carbohydrates;
            Protein = protein;
            Fat = fat;
        }

        private void ValidateInput(double carbohydrates, double protein, double fat)
        {
            if (carbohydrates < 0)
            {
                throw new ArgumentOutOfRangeException("Carbohydrate must be larger than 0");
            }

            if (protein < 0)
            {
                throw new ArgumentOutOfRangeException("Protein must be larger than 0");
            }

            if (fat < 0)
            {
                throw new ArgumentOutOfRangeException("Fat must be larger than 0");
            }

            if (carbohydrates + protein + fat > 1)
            {
                throw new ArgumentOutOfRangeException($"Sum of macronutrients cannot be larger than 1 (was {carbohydrates + protein + fat })");
            }
        }
    }
}
