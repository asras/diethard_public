﻿using System;

namespace DietAlgorithm.Model
{
    public class Ingredient : Macronutrient
    {
        public Ingredient(string name, double carbohydrates, double protein, double fat) : base(name, carbohydrates, protein, fat)
        {
        }
    }
}
