using System;
using Xunit;

namespace DietAlgorithm.Model.Api.Tests
{
    public class MacronutrientTests
    {
        [Fact]
        public void Macronutrient_ThrowsException_WhenSumOfMacrosLargerThan1()
        {
            // given
            double protein = 0.3;
            double carbohydrate = 0.4;
            double fat = 0.4;

            // expect
            Assert.True(carbohydrate + protein + fat > 1);

            // when
            Assert.Throws<ArgumentOutOfRangeException>(() => new Macronutrient(carbohydrate, protein, fat));
        }
    }
}
