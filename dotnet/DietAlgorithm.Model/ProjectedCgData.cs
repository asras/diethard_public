﻿using MathNet.Numerics.LinearAlgebra;
using System;

namespace DietAlgorithm.Model
{
    public class ProjectedCgData
    {
#pragma warning disable IDE1006 // Naming Styles
        public Vector<double> x { get; set; }
        public Vector<double> r { get; set; }
        public Vector<double> g { get; set; }
        public Vector<double> d { get; set; }
        public Matrix<double> G { get; set; }
        public Matrix<double> P { get; set; }
        public Matrix<double> A { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
