﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;

namespace DietAlgorithm.Model
{
    public class DietSystem
    {
        public Matrix<double> G { get; set; }
        public Vector<double> c { get; set; }
    }
}
