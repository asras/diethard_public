﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;

namespace DietAlgorithm.Model
{
    public class EqualityConstraint
    {
        public Matrix<double> A { get; set; }
        public Vector<double> b { get; set; }
    }
}
