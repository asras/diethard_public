﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;

namespace DietAlgorithm.Model
{
    public class QuadraticData
    {
        public Vector<double> x { get; set; }
        public IEnumerable<int> ActiveConstraintIds { get; set; }
        public Vector<double> Lambdas { get; set; }
        public Vector<double> pk { get; set; }
        public IEnumerable<InequalityConstraint> InequalityConstraints { get; set; }
        public EqualityConstraint EqualityConstraint { get; set; }
        public DietSystem System { get; set; }
    }
}
