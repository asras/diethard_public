﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;

namespace DietAlgorithm.Model
{
    public class InequalityConstraint
    {
        public Vector<double> a { get; set; }
        public double b { get; set; }
    }
}
