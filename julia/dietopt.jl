include("qpineq.jl")

struct Ingredient{T<:Real}
    carbs :: T
    protein :: T
    fat :: T
    Ingredient(carbs, protein, fat) = carbs < 0 || protein < 0 || fat < 0 ? 
    error("Inputs must larger than zero 0, got carbs = ", carbs, ", protein = ", protein, ", and fat = ", fat) : 
        carbs + protein + fat < 1 || carbs + protein + fat ≈ 1 ? 
            new{typeof(carbs)}(carbs, protein, fat) : 
            error("Sum of input should be 1 or less, got ", carbs + protein + fat)
end

struct Target{T<:Real}
    carbs :: T
    protein :: T
    fat :: T
    Target(carbs, protein, fat) = carbs < 0 || protein < 0 || fat < 0 ? 
    error("Inputs must larger than zero 0, got carbs = ", carbs, ", protein = ", protein, ", and fat = ", fat) : 
        carbs + protein + fat < 1 || carbs + protein + fat ≈ 1 ? 
            new{typeof(carbs)}(carbs, protein, fat) : 
            error("Sum of input should be 1 or less, got ", carbs + protein + fat)
end


function dietopt(ingredients :: Array{Ingredient{T}, 1}, target :: Target) where T<:Real
    A = ones(1, length(ingredients))
    b = ones(1)
    ineqas = zeros(length(ingredients), length(ingredients))
    ineqbs = zeros(length(ingredients))
    for i in 1:length(ingredients)
        ineqas[i, i] = 1
    end
    
    x0 = ones(length(ingredients)) ./ length(ingredients) ## TODO Find better start guess

    N = zeros(3, length(ingredients))
    for i in 1:length(ingredients)
        N[1, i] = ingredients[i].carbs
        N[2, i] = ingredients[i].protein
        N[3, i] = ingredients[i].fat
    end

    y = [target.carbs; target.protein; target.fat]

    G = 2.0 .* N' * N
    c = (-2.0 .* y' * N)[1, :]
    s = activesetqp(G, c, A, b, ineqas, ineqbs, x0)
    k = N * s

    result = Target(k[1], k[2], k[3])
    return s, result
end

conv(w::AbstractArray{T, 1}) where T<:Real = Ingredient(w./sum(w)...)
conv(w::AbstractArray{Array{T, 1}, 1}) where T<:Real = conv(permutedims(hcat(w...)))
function conv(w::AbstractArray{T, 2}) where T<:Real
    ingredients = Array{Ingredient{Float64}, 1}(undef, size(w, 1))
    for (i, ing) ∈ enumerate(eachrow(w))
        ingredients[i] = conv(ing)
    end
    return ingredients
end

# example declar = 50 kcal/100g
calc(declar, w) = (declar * sum(w))/100


"""
When we perform the optimization we find the optimal /nutrient/ distribution of
the ingredients. This means that if we get 0.2 carrots, 0.4 fish, etc. it means
that 20% of the /nutrients/ should come from carrots, and so on.

This is important when we want to calculate the /total weight/ or the /weight percentage/
of a given ingredient because many ingredients will have a lot of stuff that is not
nutrients, that is, stuff that does not contain carbs, protein, or fat, e.g. fibers.


Given a calorie target, CT, and an optimal nutrient distribution, ND, we can calculate
the total /nutrient weight/ we need given the calorie content per gram of carbs/protein/fat.

Define CPC as calories per gram for carbs, CPP as calories per gram for protein,
and CPF as calories per gram for fat. Then with a given optimal nutrient distribution
the calories per gram of the meal is CPM = CPC * ND(carbs) + CPP * ND(protein) + CPF * NP(fat).

To reach a calorie target we therefore need: calorie target/CPM grams of /nutrients/.

To determine the /total weight/ for a given ingredient, let us consider an example.

(The following numbers are all made up)
Of the nutrient weight in carrots there is 1/3 carbs, 1/3 protein, 1/3 fat.
Carrots also have a lot of fibers, in fact 80% of the carrot is non-nutrient fibers.
If the optimal nutrient distribution requires that we have 20% of nutrients from carrots
we get the equations

Carrot nutrient weight = 0.2 * total nutrient weight
Total carrot weight = Carrot nutrient weight / Carrot nutrient percentage = Carrot nutrient weight / 0.2


What we want is then a function:
INPUT:
- Optimal Ingredient Distribution (nutrient-wise)
- Nutrient percentage of total weight for each ingredient
- Resulting nutrient distribution
- Target calories
OUTPUT:
Optimal Ingredient Distribution (total weight-wise)


## Test cases
Let's design some test cases.

-- 1-ingredient dishes
The ingredient is Ingredient(a, b, c) with a, b, c being the nutrient percentages as usual.
The nutrient percentage is P.
The resulting nutrient distribution is of course (a, b, c).
The calories target is X.

The total nutrient weight is W =  X / (CPC * a + CPP * b + CPF * c).

The output should be a list with one element that is equal to W / P.

XXX We can define "property-tests" here, i.e. tests that randomly generate a lot of data.


-- 2-ingredient dishes
Ingredient 1 is Ingredient(a1, b1, c1), which is assumed to account for q percent of the total nutrient weight
Ingredient 2 is Ingredient(a2, b2, c2), which by assumption accounts for 1-q percent of total nutrient weight
(i.e. this means the optimizer returned the list [q, 1-q])
The nutrient percentage for ingredient 1 is P1
The nutrient percentage for ingredient 2 is P2
The resulting nutrient distribution is (A, B, C) = (a1 * q + (1-q) * a2, b1 * q + (1-q) * b2, c1 * q + (1-q) * c2)

The total nutrient weight is W =  X / (CPC * A + CPP * A + CPF * A).

The nutrient weight of Ingredient 1 should be W * q
The nutrient weight of Ingredient 2 should be W * (1-q)

The output should be a list with two elements that is equal to [W * q / P1, W * (1-q) / P2].


-- Special case tests
We should try to input recipes from Jeanette's company where the target distribution and
target calories is the distribution and target for the recipe and verify that we get back
the same weights.
"""
CALS_PER_CARB = 4 # cal per gram
CALS_PER_PROT = 4 # cal per gram
CALS_PER_FAT  = 9 # cal per gram


function convert2totalweights(ingdistribution, nutrientpercentages,
                              optimalnutrientdist, targetcals)
    @assert isapprox(sum(ingdistribution), 1.0)
    optimal_cals_per_grams = (optimalnutrientdist.carbs * CALS_PER_CARB
                              + optimalnutrientdist.protein * CALS_PER_PROT
                              + optimalnutrientdist.fat * CALS_PER_FAT)

    total_nutrient_weight = targetcals / optimal_cals_per_grams # grams
    weights = [total_nutrient_weight * q / p
               for (q, p) in zip(ingdistribution, nutrientpercentages)]

    return weights               
end


function calp2weightp(carb_p, prot_p, fat_p)
    # X CAL
    # X * carb_p calories from carbs
    # X * carb_p / CALS_PER_CARB grams of carbs

    # Weight percentages:
    # weight of carb / total weight
    # weight of carb = X * carb_p / CALS_PER_CARB
    # total weight = X * (carb_p / CALS_PER_CARB + prot_p / CALS_PER_PROT + ...)
    # carb weight percentage = carb_p / CALS_PER_CARB / (carb_p / CALS_PER_CARB + prot_p / CALS_PER_PROT + ...)

    norm = carb_p / CALS_PER_CARB + prot_p / CALS_PER_PROT + fat_p / CALS_PER_FAT
    return carb_p / (CALS_PER_CARB * norm), prot_p / (CALS_PER_PROT * norm), fat_p / (CALS_PER_FAT * norm)
end


function runoptimization(recipe, target_dist, target_cals)
    ingredients, weights = convert_yaml(recipe)

    # Sum along each ingredient i.e. get sum of weights of carbs, prots, fats
    # then divide by weight of ingredient in recipe to get which percentage
    # of the weight of the ingredient that contains nutrients
    nutrient_percentages = sum(ingredients, dims=2) ./ weights
    converted = conv(ingredients)
    s, k = dietopt(converted, target_dist)

    weights = convert2totalweights(s, nutrient_percentages, k, target_cals)
    return (k, weights)
end




"""
We want a new type of optimization that tries avoid weird ingredient distributions
by trying to stick to a given ingredient
distribution to some extent, i.e. we take 2 inputs, the ingredient distribution
and a factor lambda that determines the regularization strength.

We add an extra term to the objective function given by

lambda * ||s - s_0||^2 = lambda * (s - s_0)^T(s - s_0)
~ lambda * s^2 - 2 lambda s_0^T * s

This leads to the following changes

G -> G + 2.0 * lambda I
c -> c - 2.0 * lambda s_0
"""


function regularized_dietopt(ingredients :: Array{Ingredient{T}, 1},
                             target :: Target,
                             regularizer :: Array{T, 1},
                             lambda :: T) where T<:Real
    A = ones(1, length(ingredients))
    b = ones(1)
    ineqas = zeros(length(ingredients), length(ingredients))
    ineqbs = zeros(length(ingredients))
    for i in 1:length(ingredients)
        ineqas[i, i] = 1
    end
    
    x0 = ones(length(ingredients)) ./ length(ingredients) ## TODO Find better start guess

    N = zeros(3, length(ingredients))
    for i in 1:length(ingredients)
        N[1, i] = ingredients[i].carbs
        N[2, i] = ingredients[i].protein
        N[3, i] = ingredients[i].fat
    end

    y = [target.carbs; target.protein; target.fat]

    G = 2.0 .* N' * N + 2.0 * lambda * I
    c = (-2.0 .* y' * N)[1, :] - 2.0 * lambda .* regularizer
    s = activesetqp(G, c, A, b, ineqas, ineqbs, x0)
    k = N * s

    result = Target(k[1], k[2], k[3])
    return s, result
end


function run_reg_opt(recipe, target_dist, target_cals,
                     regularization_dist, regularization_strength)
    ingredients, weights = convert_yaml(recipe)

    # Sum along each ingredient i.e. get sum of weights of carbs, prots, fats
    # then divide by weight of ingredient in recipe to get which percentage
    # of the weight of the ingredient that contains nutrients
    nutrient_percentages = sum(ingredients, dims=2) ./ weights
    
    for i in 1:length(regularization_dist)
        regularization_dist[i] *= nutrient_percentages[i]
    end
    regularization_dist ./= sum(regularization_dist)
    
    converted = conv(ingredients)
    s, k = regularized_dietopt(converted, target_dist,
                               regularization_dist, regularization_strength)

    weights = convert2totalweights(s, nutrient_percentages, k, target_cals)
    return (k, weights)
end



