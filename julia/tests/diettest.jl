import YAML
include("../dietopt.jl")
include("../utils.jl")

# Load test data
data = YAML.load_file("recipes.yml")

# # Example ingredients lookup
# data["Ymer med drys"]["ingredients"][1]["carbs"]
 

function test_recipe(ingredients)

    target = Target(0.5, 0.3, 0.2)

    s, k = dietopt(conv(ingredients), target)

    @show s
    @show k

    @assert isapprox(sum(s), 1.0) "Sum was not 1.0: $(sum(s))"
end


"""
Vi forventer: Hvis vi tager en Coachify opskrift (ingredienserne (vægt), target (prot/carb/fat), target kcal) og kører den igennem vores optimeringsalgoritme, så vil vi få noget som er næsten det samme som Coachify's vægtfordeling, dvs. næsten den samme opskrift.

- Skrive en funktion som udregner hvor tæt på Coachify's opskrift er på målet
"""

# recipe = data["Asian one-pot pasta"]
function close_to_coachify(data, recipe) # -> [deres vægte], [vores vægte] eller udregne en eller anden forskel

    printstyled("Testing recipe: $recipe\n", color=:blue)
    ingredients, coachify_weights = convert_yaml(data, recipe)
    nutrient_weights = sum(ingredients, dims=2)
    nutrient_percentages = nutrient_weights ./ coachify_weights

    target_kcals = data[recipe]["targets"]["kcal"]
    #target = data[recipe]["targets"]["distribution"]
    #target = "0.4 0.4 0.2"
    target = "0.3 0.5 0.2"
    #target = "0.25 0.05 0.70"
    parsed = [parse(Float64, x) for x in split(target, ' ')]
    carbp, protp, fatp = calp2weightp(parsed[2], parsed[1], parsed[3])
    target = Target(carbp, protp, fatp)

    s, k = dietopt(conv(ingredients), target)

    our_weights = convert2totalweights(s, nutrient_percentages, k, target_kcals)

    println("Our weights = ", round.(our_weights, digits=1))
    println("Coachify weights =", coachify_weights)
    @show target
    @show k

    println("----------------------------------------")
end

#close_to_coachify(data, "Ymer med drys")
#close_to_coachify(data, "Mortens ynglings ret - Paprika gryde")
close_to_coachify(data, "Millionboef")
#close_to_coachify(data, "Bananapancakes")
#close_to_coachify(data, "Asian one-pot pasta")
#close_to_coachify(data, "Fitness chicken with rice")


function test_coachify_numbers(data, recipe)
    # Test if we get same nutrient distribution
    # when we calculate it from the recipe
    # as Coachify has indicated

    coachify_dist = [parse(Float64, x) for x in split(data[recipe]["targets"]["distribution"], ' ')]
    ingredients, coachify_weights = convert_yaml(data, recipe)

    total_nutrient_weight = sum(ingredients)
    total_nutrients = sum(ingredients, dims=1)

    CALS_PER_CARB = 4 # cal per gram
    CALS_PER_PROT = 4 # cal per gram
    CALS_PER_FAT  = 9 # cal per gram
    total_kcals = zeros(size(total_nutrients))
    total_kcals[1] = total_nutrients[1] * CALS_PER_CARB
    total_kcals[2] = total_nutrients[2] * CALS_PER_PROT
    total_kcals[3] = total_nutrients[3] * CALS_PER_FAT
        
    percentages = total_kcals ./ sum(total_kcals)

    @show sum(total_kcals)
    println("Coachify's target kcal: 400")
    @show percentages
    @show coachify_dist

end

#test_coachify_numbers(data, "Mortens ynglings ret - Paprika gryde")


function weight_conv_1_ing()
    for i in 1:100
        nutrient_percentage = rand()
        x = rand()
        y = rand()
        optimal_nutrient_distribution = Target(x, (1-x) * y, (1-x)*(1-y))
        target_weight = 100 # We are cheating and saying we definitely want 100g of nutrients to make it easier to check result
        target_calories = target_weight * x * CALS_PER_CARB + target_weight * (1-x)*y*CALS_PER_PROT + target_weight*(1-x)*(1-y)*CALS_PER_FAT
        answer = convert2totalweights([1.0], [nutrient_percentage],
                                      optimal_nutrient_distribution,
                                      target_calories)
        @assert length(answer) == 1
        expected = target_weight/nutrient_percentage
        @assert isapprox(answer[1], expected) "Answer: $(answer[1]), expected: $(expected)"
    end
end



coachify_dist(data["Millionboef"])


# Dont really need a macro for this, but it's fun
# Can use macroexpand to see the code a macro generates
# @macroexpand @testfunc test_recipe
macro testfunc(func)
    return :( begin
    print("Running test $(Symbol($func))...")
    try
        $func()
        printstyled("passed!\n", color=:green)
    catch e
        printstyled("failed!\n", color=:red)
        throw(e)
    end
    end)
end


# @testfunc test_recipe
# @testfunc weight_conv_1_ing
