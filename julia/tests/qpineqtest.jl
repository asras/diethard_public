include("../qpineq.jl")


G = [2.0 0.0; 0.0 2.0]
c = [-2.0; -5.0]
A = [1.0 -2.0;
     -1 -2;
     -1 2;
     1 0;
     0 1]
b = [-2.0; -6; -2; 0; 0]


x = [2.0; 0.0]
actives = [1; 2]
lambdas = [0.0; 0.0; 0.0; 0.0; 1.0]
pk = [0.0; 0.0]

con1 = InEqConstraint([1.0; -2.0], -2.0)
con2 = InEqConstraint([-1.0; -2.0], -6.0)
con3 = InEqConstraint([-1.0; 2.0], -2.0)
con4 = InEqConstraint([1.0; 0.0], 0.0)
con5 = InEqConstraint([0.0; 1.0], 0.0)

A2 = zeros(0, 2)
b2 = zeros(0)

eqs = EqConstraints(A2, b2)
sys = System(G, c)

data = QPData(x, actives, lambdas, pk, [con1, con2, con3, con4, con5], eqs, sys)

active = getactiveA(data)
@assert isapprox(active, hcat(con1.a, con2.a)')
@assert isapprox(map(c -> c[2].a, getinactive(data)), [[-1.0, 2.0], [1.0, 0.0], [0.0, 1.0]])


@show data.lambdas
qpstep!(data)
@show data.lambdas

## Example from Nocedal
print("\nRunning test from Nocedal...")

x = [2.0; 0.0]
actives = [3; 5]
lambdas = [0.0]
pk = [0.0; 0.0]

con1 = InEqConstraint([1.0; -2.0], -2.0)
con2 = InEqConstraint([-1.0; -2.0], -6.0)
con3 = InEqConstraint([-1.0; 2.0], -2.0)
con4 = InEqConstraint([1.0; 0.0], 0.0)
con5 = InEqConstraint([0.0; 1.0], 0.0)

A2 = zeros(0, 2)
b2 = zeros(0)

eqs = EqConstraints(A2, b2)
sys = System(G, c)

data = QPData(x, actives, lambdas, pk, [con1, con2, con3, con4, con5], eqs, sys)


qpstep!(data)
@assert isapprox(data.pk, zeros(size(data.pk)))
@assert isapprox(data.lambdas, [-2.0; -1.0])
@assert isapprox(data.x, [2.0; 0.0])
@assert length(data.activeconstraints) == 1 && 5 in data.activeconstraints
qpstep!(data)
@assert isapprox(data.pk, [-1.0; 0.0])
@assert isapprox(data.x, [1.0; 0.0])
@assert length(data.activeconstraints) == 1 && 5 in data.activeconstraints
qpstep!(data)
@assert isapprox(data.pk, [0.0; 0.0])
@assert isapprox(data.x, [1.0; 0.0])
@assert length(data.lambdas) == 1 && isapprox(data.lambdas[1], -5.0)
@assert length(data.activeconstraints) == 0
qpstep!(data)
@assert isapprox(data.pk, [0.0; 2.5])
@assert isapprox(data.x, [1.0; 1.5])
@assert length(data.activeconstraints) == 1 && data.activeconstraints[1] == 1
qpstep!(data)
@assert isapprox(data.pk, [0.4; 0.2])
@assert isapprox(data.x, [1.4; 1.7])
@assert length(data.activeconstraints) == 1 && data.activeconstraints[1] == 1
qpstep!(data)
@assert isapprox(data.pk, [0.0; 0.0])
@assert isapprox(data.lambdas[1], 0.8)
@assert isapprox(data.x, [1.4; 1.7])

println("Passed!")






### GET BLOCKING CONSTRAINT TEST

