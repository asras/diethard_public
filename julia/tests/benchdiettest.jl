using BenchmarkTools

include("../dietopt.jl")

function main()
    target = Target(0.5, 0.3, 0.2)
    ingredients = conv([
        4.4 0.4 0.2; # carrots
        12.3 4.1 1.4; # chickpeas, dry
        1.9 0.5 0.1; # leeks
        1.7 11.4 1.8; # seitan
        3.1 0.4 5.2; # coconut milk
        22.5 4.4 0.4; # pasta
        3.0 3.3 1.5; # edamammamamMUMS beans
    ])

    @btime dietopt($ingredients, $target)
end

main()