include("../qpeq.jl")

G = [6 2 1; 2 5 2; 1 2 4.]
c = [-8;-3;-3.]
A = [1 0 1; 0 1 1.]
b = [3;0.]
x0 = [4.0;1.0;-1.0]
@show A * x0 - b

x = qpoptimize(G, c, A, b, 1e-6, x0)


@show x # print(f"x = {x}")
@show A * x - b
