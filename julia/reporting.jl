"""
Module containing functions for summarizing results

report take a recipe and calculates
- Actual Fat/Carb/Protein distribution
- Optimized ingredient distribution for
    - Target dist (usually 0.4/0.4/0.2)
    - Keto dist (0.25/0.5/0.25 or something)
    - Weight gain dist (0.5/0.3/0.2)
"""

using Printf
import YAML
include("utils.jl")
include("dietopt.jl")

function report(recipe) ## TODO Make a type for recipe?
    actual_dist = actual_dist(recipe)
    actual_weights = [ing["weight"] for ing in recipe["ingredients"]]
    
    targets = [(s, Target(calp2weightp(x...)...))
               for (s, x) in
               [("Weight loss", (0.4, 0.4, 0.2)),
                ("Keto", (0.25, 0.5, 0.25)),
                ("Weight gain", (0.5, 0.3, 0.2))]]
    target_cals = recipe["targets"]["kcal"]

    regularization_dist = actual_weights
    opt_results = [(t[1], t[2],
                    [runoptimization(recipe, t[2], target_cals),
                    run_reg_opt(recipe, t[2], target_cals,
                                copy(regularization_dist), 0.1),
                    run_reg_opt(recipe, t[2], target_cals,
                                copy(regularization_dist), 0.2),
                    run_reg_opt(recipe, t[2], target_cals,
                                copy(regularization_dist), 0.5),
                     run_reg_opt(recipe, t[2], target_cals,
                                 copy(regularization_dist), 50.0),
                     run_reg_opt(recipe, t[2], target_cals,
                                 copy(regularization_dist), 500.0)]) for t in targets]

    
    return (actual_dist, target_cals, opt_results, actual_weights)
end


function printweights(names, weights1, weightlist)
    w = maximum([length(n) for n in names])
    sp = "   "
    println(rpad("", w, " "), "  Initial", sp, "Optimized")
    w_ = length("  Actual")
    for (i, (n, w1)) in enumerate(zip(names, weights1))
        print(rpad(n, w, " "),
                ": ",
                rpad(string(round(w1, digits=0)), w_, " "),
              " ")
        for weights in weightlist
            print(rpad(string(round(weights[i], digits=0)), 8, " "))
        end
        print("\n")
    end
end


function print_report(rows, data)
    for (recipename, actual_dist, target_cals, opt_results, actual_weights, names) in rows
        println("###################################################")
        println("                     $recipename             ")
        println("###################################################")
        for (targettype, target, optlist) in opt_results
            dists = [x[1] for x in optlist]
            weightslist = [x[2] for x in optlist]
            # (opt_dist, weights) = opt
            # (reg_dist1, reg_weights1) = reg_opt1
            # (reg_dist2, reg_weights2) = reg_opt2
            # (reg_dist3, reg_weights3) = reg_opt3
            println("-----------------------------------------------")
            println("Target type: $(uppercase(targettype))")
            print("With target: ")
            @printf "%0.3f %0.3f %0.3f\n" target.carbs target.protein target.fat
            print("Opt. dists:\n")
            for dist in dists
                @printf "%0.3f %0.3f %0.3f\n" dist.carbs dist.protein dist.fat
            end
            # print("Reg. Opt. dist 1:   ")
            # @printf "%0.3f %0.3f %0.3f\n" reg_dist1.carbs reg_dist1.protein reg_dist1.fat
            
            # print("Reg. Opt. dist 2:   ")
            # @printf "%0.3f %0.3f %0.3f\n" reg_dist2.carbs reg_dist2.protein reg_dist2.fat

            # print("Reg. Opt. dist 3:   ")
            # @printf "%0.3f %0.3f %0.3f\n" reg_dist3.carbs reg_dist3.protein reg_dist3.fat

            println("")
            # println("Opt. weights:  ", round.(weights, digits=0))
            # println("Actual weights: ", round.(actual_weights, digits=0))
            printweights(names, actual_weights, weightslist)
        end

        println("-----------------------------------------------")
        println("")
        (tp, tc, tf) = split(data[recipename]["targets"]["distribution"], " ")
        (tc, tp, tf) = calp2weightp(parse(Float64,tc), parse(Float64,tp), parse(Float64, tf))
        println("               CARBS PROT  FAT")
        print("Proposed dist: ")
        @printf "%0.3f %0.3f %0.3f\n" (tc, tp, tf)...
        print("Actual dist:   ")
        @printf "%0.3f %0.3f %0.3f\n" actual_dist...
        print("\n\n\n")
    end
end


## TODO Include ingredient names

function makereport()
    data = YAML.load_file("tests/recipes.yml")
    rows = []

    for recipe in keys(data)
        if recipe == "notes"
            continue
        end
        (actual_dist, target_cals, opt_results, actual_weights) = report(data[recipe])
        names = [ing["name"] for ing in data[recipe]["ingredients"]]
        push!(rows, [recipe, actual_dist, target_cals, opt_results, actual_weights, names])
    end
    print_report(rows, data)
    return
end
