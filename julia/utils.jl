function convert_yaml(data, recipe) # TODO Remove this
    printstyle("WARNING: THIS IS DEPRECATED\n", color=:red)
    printstyle("WARNING: THIS IS DEPRECATED\n", color=:blue)
    printstyle("WARNING: THIS IS DEPRECATED\n", color=:green)
    printstyle("WARNING: THIS IS DEPRECATED\n", color=:black)

   arr_ing = zeros(length(data[recipe]["ingredients"]), 3)
   arr_w = zeros(length(data[recipe]["ingredients"]), 1)   

   for (ind, val) in enumerate(data[recipe]["ingredients"])
       arr_ing[ind, 1] = val["carbs"]
       arr_ing[ind, 2] = val["protein"]
       arr_ing[ind, 3] = val["fat"]
       arr_w[ind] = val["weight"]
   end

   return arr_ing, arr_w
end

function convert_yaml(recipe)

   arr_ing = zeros(length(recipe["ingredients"]), 3)
   arr_w = zeros(length(recipe["ingredients"]), 1)   

   for (ind, val) in enumerate(recipe["ingredients"])
       arr_ing[ind, 1] = val["carbs"]
       arr_ing[ind, 2] = val["protein"]
       arr_ing[ind, 3] = val["fat"]
       arr_w[ind] = val["weight"]
   end

   return arr_ing, arr_w
end



function actual_dist(recipe)

    p_nut = []
    c_nut = []
    f_nut = []
    w_nut = []

    for nut in recipe["ingredients"]
        push!(p_nut, nut["protein"])
        push!(c_nut, nut["carbs"])
        push!(f_nut, nut["fat"])
        push!(w_nut, nut["protein"] + nut["carbs"] + nut["fat"])        
        
    end

    dist_p = round(sum(p_nut)/sum(w_nut), digits=3)
    dist_c = round(sum(c_nut)/sum(w_nut), digits=3)
    dist_f = round(sum(f_nut)/sum(w_nut), digits=3)
    @assert isapprox(dist_p + dist_c + dist_f, 1.0)
    return (dist_c, dist_p, dist_f)
end
