using LinearAlgebra
include("qpeq.jl")

struct InEqConstraint{T<:Real}
    a :: Array{T, 1}
    b :: T
end

struct EqConstraints{T<:Real}
    A :: Array{T, 2}
    b :: Array{T, 1}
end

struct System{T<:Real}
    G :: Array{T, 2}
    c :: Array{T, 1}
end
    

mutable struct QPData{T<:Real}
    x :: Array{T, 1}
    activeconstraints :: Array{Int, 1}
    lambdas :: Array{T, 1}
    pk :: Array{T, 1}
    ineqconstraints :: Array{InEqConstraint{T}, 1}
    eqconstraints :: EqConstraints{T}
    system :: System{T}
end


function getactiveA(data; onlyInEq = false)
    activeInEqs = data.ineqconstraints[data.activeconstraints]

    N = length(data.activeconstraints) # + size(data.eqconstraints.A)[1]

    A = zeros(N, size(data.x, 1))

    for i ∈ 1:N
        A[i, :] = activeInEqs[i].a
    end

    return onlyInEq ? A : vcat(A, data.eqconstraints.A)
end


function getinactive(data)
    N = length(data.ineqconstraints) - length(data.activeconstraints)
    A = []
    for i ∈ 1:length(data.ineqconstraints)
        if i ∉ data.activeconstraints
            push!(A, (i, data.ineqconstraints[i]))
        end
    end

    return A
end


function getblockingconstraints(constraints, x, pk)
    alpha = 1
    index = -1
    for (i, c) ∈ constraints
        if c.a' * pk ≥ 0
            continue
        end
        nalpha = (c.b - c.a' * x) / (c.a' * pk)
        if nalpha < alpha
            alpha = nalpha
            index = i
        end
    end

    return (index, min(1, alpha))
end


approxzero(lambdas) = isapprox(lambdas, zeros(size(lambdas)))


function qpstep!(data :: QPData)
    G = data.system.G
    c = data.system.c
    gvec = G * data.x + c

    activeA = getactiveA(data, onlyInEq=false)

    npk = qpoptimize(G, gvec, activeA, zeros(size(activeA, 1))) # TODO Put threshold
    data.pk = npk

    if approxzero(npk)
        activeInEqA = getactiveA(data, onlyInEq=true)
        lambdas = (activeA' \ gvec)
        lambdas = lambdas[1:length(lambdas)-size(data.eqconstraints.A, 1)]
        data.lambdas = lambdas
        
        if !approxzero(lambdas)
            deleteat!(data.activeconstraints, argmin(lambdas))
        end
    else
        inactives = getinactive(data)
        (blockingindex, alpha) = getblockingconstraints(inactives, data.x, npk)
        data.x = data.x + alpha * npk
        if !(alpha ≈ 1.0)
            push!(data.activeconstraints, blockingindex)
        end
    end    
end


function converge!(data :: QPData)
    while true
        qpstep!(data)
        if approxzero(data.pk) && all(>=(0), data.lambdas)
            return data.x
        end
    end
end


function checkconstraints(data :: QPData, x0)
    for ineq in data.ineqconstraints
        @assert ineq.a' * x0 >= ineq.b "LHS: $(ineq.a' * x0), RHS: $(ineq.b)"
    end
    eqA = data.eqconstraints.A
    eqb = data.eqconstraints.b
    @assert isapprox(eqA * x0, eqb) "LHS: $(eqA * x0), RHS: $(eqb)"
end


function activesetqp(G, c, eqA, eqb, ineqas, ineqbs, x0)
    ineqs = Array{InEqConstraint{Float64}, 1}(undef, size(ineqbs, 1))
    for i ∈ 1:length(ineqbs) # (a, b) in zip(ineqas, ineqbs)
        a = ineqas[i, :]
        b = ineqbs[i]
        ineqs[i] = InEqConstraint(a, b)
    end

    eqs = EqConstraints(eqA, eqb)

    sys = System(G, c)

    actives :: Array{Int, 1} = []
    for (i, ineq) in enumerate(ineqs)
        if ineq.a' * x0 - ineq.b ≈ 0.0
            push!(i)
        end
    end

    data = QPData(x0, actives, [0.0], zeros(size(x0)), ineqs, eqs, sys)
    checkconstraints(data, x0)

    x = converge!(data)

    return x
end








    

                                                          
