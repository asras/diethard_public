using LinearAlgebra

mutable struct PCGData{T<:Real}
    x :: Array{T, 1}
    r :: Array{T, 1}
    g :: Array{T, 1}
    d :: Array{T, 1}
    G :: Array{T, 2}
    P :: Array{T, 2}
    A :: Array{T, 2}
end

function calculate_alpha(data, threshold)
    if abs(transpose(data.d) * data.G * data.d) < threshold
        return 0.0
    else
        return transpose(data.r) * data.g / (transpose(data.d) * data.G * data.d)
    end
end


function pcgstep!(data :: PCGData, threshold = 1e-8)
    alpha = calculate_alpha(data, threshold)
    x = data.x + alpha * data.d
    rp = data.r + alpha * data.G * data.d
    gp = data.P * rp
    beta = transpose(rp) * gp / (transpose(data.r) * data.g)
    d = -gp + beta * data.d
    g = gp
    r = rp
    data.x = x
    data.r = r
    data.g = g
    data.d = d
end



function converge!(data :: PCGData, threshold :: Number)
    error = 1.0
    while true
        if error < threshold
            break
        end

        pcgstep!(data, threshold)
        error = transpose(data.r) * data.g
    end
end


function qpoptimize(G, c, A, b, threshold = 1e-4, x = nothing)
    if x === nothing
        x = A \ b
    end
    r = G * x + c
    P = I - transpose(A) * inv(A * transpose(A)) * A
    g = P * r
    d = -g
    
    data = PCGData(x, r, g, d, G, P, A)

    converge!(data, threshold)

    return data.x
end
